/***********************************************
AUTOMATION ASSIGNEE: Jerin
DESCRIPTION: practice tests for Crowdlinker website 'General Inquiries'
***********************************************/

describe('Filling form information', function () {
    beforeEach(function () {
        cy.viewport('macbook-13')
    })

    //***************** filling in some general inquiry information *********************
    it('Filling in test information for inquiry', function () {
        cy.visit('https://staging.crowdlinker.com/contact/general-inquiries/')

        // complete inquiry information
        cy.get("input[name='fullName']")
            .type("Testing")

        cy.get("input[name='email']")
            .type("dev@crowdlinker.com")

        cy.get("input[name='referral']")
            .type("Just Testing This Out")

        cy.get("input[name='phoneNumber']")
            .type("0000000000")

        cy.get("textarea[name='description']")
            .type("Just Testing This Out")

        // cy.wait(1000)
        // cy.get('input[name=newsLetterSignup]').type('checkbox').check({ force: true }).should('be.checked') 
        cy.contains("Submit").click()
    })
})
